package net.pl3x.pl3xlocks;

public class Lock {
	private int id;
	private String owner; // uuid
	private int dimension;
	private int x;
	private int y;
	private int z;

	public Lock(int id, String owner, int dimension, int x, int y, int z) {
		this.id = id;
		this.owner = owner;
		this.dimension = dimension;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	@Override
	public String toString() {
		return "Lock [id=" + id + ",owner=" + owner + ",dimension=" + dimension + ",x=" + x + ",y=" + y + ",z=" + z + "]";
	}
}
