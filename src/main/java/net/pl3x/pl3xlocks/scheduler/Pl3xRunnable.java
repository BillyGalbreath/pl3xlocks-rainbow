package net.pl3x.pl3xlocks.scheduler;

import Pl3xLocks.MyPlugin;

public class Pl3xRunnable {
	protected MyPlugin plugin;
	private Integer id = -1;
	private Integer delay = -1;
	private Integer curr_tick = -1;

	public Pl3xRunnable(MyPlugin plugin) {
		this.plugin = plugin;
	}

	public void run() {
	}

	public Integer startTask(Integer delay) {
		this.delay = delay;
		this.curr_tick = 0;
		return plugin.getScheduler().addRunnable(this);
	}

	public void cancel() {
		plugin.getScheduler().removeRunnable(id);
		id = 0;
		delay = 0;
		curr_tick = -1;
	}

	protected void doTick() {
		if (curr_tick < 0) {
			return; // task is not currently scheduled
		}
		curr_tick++;
		if (curr_tick < delay) {
			return; // not enough delay to run
		}
		curr_tick = 0;
		run();
	}
}
