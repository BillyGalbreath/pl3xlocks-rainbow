package net.pl3x.pl3xlocks.scheduler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Scheduler {
	private HashMap<Integer, Pl3xRunnable> runnables = new HashMap<Integer, Pl3xRunnable>();
	private Integer next = 1;

	protected Integer addRunnable(Pl3xRunnable runnable) {
		if (runnables.containsValue(runnable)) {
			return -1;
		}
		Integer id = next++;
		runnables.put(id, runnable);
		return id;
	}

	protected void removeRunnable(Integer id) {
		if (!runnables.containsKey(id)) {
			return;
		}
		for (Iterator<Map.Entry<Integer, Pl3xRunnable>> iter = runnables.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<Integer, Pl3xRunnable> entry = iter.next();
			if (entry.getKey().equals(id)) {
				iter.remove();
			}
		}
	}

	public Pl3xRunnable getRunnable(int id) {
		if (!runnables.containsKey(id)) {
			return null;
		}
		return runnables.get(id);
	}

	public void cancelAllRunnables() {
		for (Iterator<Map.Entry<Integer, Pl3xRunnable>> iter = runnables.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<Integer, Pl3xRunnable> entry = iter.next();
			entry.getValue().cancel();
		}
	}

	public Integer scheduleTask(Pl3xRunnable runnable, int delay) {
		runnable.run(); // run once before scheduling
		return runnable.startTask(delay);
	}

	public void tick() {
		for (Pl3xRunnable runnable : runnables.values()) {
			runnable.doTick();
		}
	}
}
