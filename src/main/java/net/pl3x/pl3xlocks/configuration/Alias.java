package net.pl3x.pl3xlocks.configuration;

import java.util.Arrays;
import java.util.List;

import Pl3xLocks.MyPlugin;

public enum Alias {
	LOCK("lock", "protect"),
	UNLOCK("unlock", "unprotect");

	private String key;
	private String def;

	private BaseConfig config;

	private Alias(String key, String def) {
		this.key = key;
		this.def = def;
		config = new BaseConfig(MyPlugin.getInstance(), "aliases.ini");
	}

	public List<String> get() {
		String value = config.get(key, def).replace(" ", "");
		if (value == null || value.equals("")) {
			// This shouldn't happen
			return Arrays.asList(new String[] {});
		}
		return Arrays.asList(value.split(","));
	}
}
