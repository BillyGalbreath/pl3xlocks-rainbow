package net.pl3x.pl3xlocks.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import net.pl3x.pl3xlocks.Utils;
import Pl3xLocks.MyPlugin;
import PluginReference.MC_Location;

public class BaseConfig {
	protected MyPlugin plugin;
	protected File configFile;
	protected ConfigProperties properties;
	protected String fileName;
	protected String pathFile;

	public BaseConfig(MyPlugin plugin, String file) {
		this.plugin = plugin;
		if (file == null || file.equals("")) {
			file = "config.ini";
		}
		if (file.contains(File.separator)) {
			String[] bits = file.split(Pattern.quote(File.separator));
			fileName = bits[bits.length - 1];
		} else {
			fileName = file;
		}
		pathFile = file;
		init();
		reload();
	}

	public void init() {
		configFile = new File(plugin.getPluginDir(), pathFile);
		if (!configFile.exists()) {
			configFile.getParentFile().mkdirs();
			// copy default file from jar to disk
			TextFile.copyFileFromJarToDisk(plugin, fileName);
		}
		if (!configFile.exists()) {
			// create new empty file if still does not exist
			try {
				configFile.createNewFile();
			} catch (IOException e) {
				plugin.getLogger().error("Failed to create configuration file: " + configFile.getAbsoluteFile());
			}
		}
	}

	protected void reload() {
		properties = new ConfigProperties();
		FileInputStream in;
		try {
			in = new FileInputStream(configFile);
			properties.load(in);
			in.close();
		} catch (Exception e) {
			plugin.getLogger().error("Failed to load configuration file: " + configFile.getAbsoluteFile());
			e.printStackTrace();
		}
	}

	public String get(String key) {
		return get(key, null);
	}

	public String get(String key, String def) {
		String raw = properties.getProperty(key);
		if (raw == null) {
			return def;
		}
		return raw;
	}

	public void set(String key, String value) {
		try {
			properties = new ConfigProperties();
			FileInputStream in = new FileInputStream(configFile);
			properties.load(in);
			in.close();
			properties.put(key, value);
			FileOutputStream out = new FileOutputStream(configFile);
			properties.store(out, null);
			out.close();
		} catch (Exception e) {
			plugin.getLogger().error("Failed to save default config.ini file: ");
			e.printStackTrace();
		}
	}

	public Integer getInteger(String key) {
		return getInteger(key, null);
	}

	public Integer getInteger(String key, Integer def) {
		String raw = get(key);
		if (raw == null) {
			return def;
		}
		Integer theInt;
		try {
			theInt = Integer.valueOf(raw);
		} catch (NumberFormatException e) {
			return def; // not a number!
		}
		return theInt;
	}

	public Double getDouble(String key) {
		return getDouble(key, null);
	}

	public Double getDouble(String key, Double def) {
		String raw = get(key);
		if (raw == null) {
			return def;
		}
		Double theDouble;
		try {
			theDouble = Double.valueOf(raw);
		} catch (NumberFormatException e) {
			return def; // not a number!
		}
		return theDouble;
	}

	public boolean getBoolean(String key) {
		return getBoolean(key, false);
	}

	public boolean getBoolean(String key, boolean def) {
		String raw = get(key);
		if (raw == null) {
			return def;
		}
		return raw.equalsIgnoreCase("true");
	}

	public List<String> getStringList(String key) {
		return getStringList(key, ",");
	}

	public List<String> getStringList(String key, String separator) {
		String raw = get(key);
		if (raw == null) {
			return null;
		}
		return Arrays.asList(raw.split(separator));
	}

	public MC_Location getLocation(String key) {
		return getLocation(key, null);
	}

	public MC_Location getLocation(String key, MC_Location def) {
		String raw = get(key);
		if (raw == null) {
			return def;
		}
		return Utils.stringToLocation(raw);
	}

	public void setLocation(String key, MC_Location location) {
		set(key, location.x + "," + location.y + "," + location.z + "," + location.dimension + "," + location.yaw + "," + location.pitch);
	}

	public HashMap<String, String> getMap(String key) {
		String raw = get(key);
		HashMap<String, String> map = new HashMap<String, String>();
		if (raw == null) {
			return map;
		}
		for (String tool : raw.split("\u256C")) {
			String[] split = tool.split("\u2261");
			if (split.length != 2) {
				continue;
			}
			map.put(split[0], split[1]);
		}
		return map;
	}

	public void setMap(String key, HashMap<String, String> map) {
		String raw = "";
		for (Iterator<Map.Entry<String, String>> iter = map.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<String, String> entry = iter.next();
			if (raw != null & !raw.equals("")) {
				raw += "\u256C";
			}
			raw += entry.getKey() + "\u2261" + entry.getValue();
		}
		set(key, raw);
	}
}
