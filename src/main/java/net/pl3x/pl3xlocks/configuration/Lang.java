package net.pl3x.pl3xlocks.configuration;

import Pl3xLocks.MyPlugin;

public enum Lang {
	ERROR_UNKNOWN("error-unknown", "&4An unknown error happened!"),
	ERROR_NO_PERM("error-no-permission", "&4You do not have permission for that command!"),
	ERROR_NO_CONSOLE("error-no-console", "&4This command expects a player!"),
	ERROR_PLAYER_NOT_FOUND("error-player-not-found", "&4Player not found!"),

	LOCK_HELP_DESC("lock-help-desc", "Protect a block with a lock");

	private String key;
	private String def;

	private BaseConfig config;

	private Lang(String key, String def) {
		this.key = key;
		this.def = def;
		config = new BaseConfig(MyPlugin.getInstance(), MyPlugin.getInstance().getConfig().get("language-file", "lang-en.ini"));
	}

	public String get() {
		String value = config.get(key, def);
		if (value == null) {
			// This shouldn't happen
			value = "&c[missing lang data]";
		}
		return value;
	}
}
