package net.pl3x.pl3xlocks;

import Pl3xLocks.MyPlugin;

public class Logger {
	MyPlugin plugin;

	public Logger(MyPlugin plugin) {
		this.plugin = plugin;
	}

	public void info(String message) {
		System.out.println("[" + plugin.getPluginName() + "] " + message);
	}

	public void error(String message) {
		System.out.println("[" + plugin.getPluginName() + "] [!!! ERROR !!!]" + message);
	}

	public void debug(String message) {
		if (plugin.getConfig().getBoolean("debug-mode")) {
			System.out.println("[" + plugin.getPluginName() + "] [DEBUG] " + message);
		}
	}
}
