package net.pl3x.pl3xlocks;

import java.util.Arrays;
import java.util.List;

import PluginReference.MC_Location;

public class Utils {
	public static String join(String[] list, String joiner, int start) {
		return join(Arrays.asList(list), joiner, start);
	}

	public static String join(List<String> list, String joiner, int start) {
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < list.size(); i++) {
			if (i != start) {
				bldr.append(joiner);
			}
			bldr.append(list.get(i));
		}
		return bldr.toString();
	}

	public static MC_Location stringToLocation(String string) {
		String[] rawSplit = string.split(",");
		double x, y, z;
		int dimension;
		float yaw, pitch;
		try {
			x = Double.valueOf(rawSplit[0]);
			y = Double.valueOf(rawSplit[1]);
			z = Double.valueOf(rawSplit[2]);
			dimension = Integer.valueOf(rawSplit[3]);
			yaw = Float.valueOf(rawSplit[4]);
			pitch = Float.valueOf(rawSplit[5]);
		} catch (Exception e) {
			return null;
		}
		return new MC_Location(x, y, z, dimension, yaw, pitch);
	}

	public static String locationToString(MC_Location location) {
		return location.x + "," + location.y + "," + location.z + "," + location.dimension + "," + location.yaw + "," + location.pitch;
	}
}
