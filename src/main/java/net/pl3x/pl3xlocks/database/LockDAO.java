package net.pl3x.pl3xlocks.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.pl3x.pl3xlocks.Lock;
import Pl3xLocks.MyPlugin;
import PluginReference.MC_Location;

public class LockDAO {
	private MyPlugin plugin;
	private Connection connection = null;
	private PreparedStatement ptmt = null;
	private ResultSet resultSet = null;

	public LockDAO(MyPlugin plugin) {
		this.plugin = plugin;
	}

	public void insert(Lock lock) {
		String sql = "INSERT INTO lock (ID, OWNER, DIMENSION, X, Y, Z) VALUES (?, ?, ?, ?, ?, ?)";
		try {
			connection = plugin.getDatabase().getConnection();
			ptmt = connection.prepareStatement(sql);
			ptmt.setInt(1, lock.getId());
			ptmt.setString(2, lock.getOwner());
			ptmt.setInt(3, lock.getDimension());
			ptmt.setInt(4, lock.getX());
			ptmt.setInt(5, lock.getY());
			ptmt.setInt(6, lock.getZ());
			ptmt.executeUpdate();
		} catch (SQLException e) {
			plugin.getLogger().error("Problem trying to save lock: " + e.getLocalizedMessage());
		} finally {
			try {
				if (ptmt != null) {
					ptmt.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException ignore) {
			}
		}
	}

	public void update(Lock lock) {
		String sql = "UPDATE lock SET OWNER=?, DIMENSION=?, X=?, Y=?, Z=? WHERE ID=?";
		try {
			connection = plugin.getDatabase().getConnection();
			ptmt = connection.prepareStatement(sql);
			ptmt.setString(1, lock.getOwner());
			ptmt.setInt(2, lock.getDimension());
			ptmt.setInt(3, lock.getX());
			ptmt.setInt(4, lock.getY());
			ptmt.setInt(5, lock.getZ());
			ptmt.setInt(6, lock.getId());
			ptmt.executeUpdate();
		} catch (SQLException e) {
			plugin.getLogger().error("Problem trying to update lock: " + e.getLocalizedMessage());
		} finally {
			try {
				if (ptmt != null) {
					ptmt.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException ignore) {
			}
		}
	}

	public void delete(Lock lock) {
		String sql = "DELETE FROM lock WHERE DIMENSION=? AND X=? AND Y=? AND Z=?";
		try {
			connection = plugin.getDatabase().getConnection();
			ptmt = connection.prepareStatement(sql);
			ptmt.setString(1, lock.getOwner());
			ptmt.setInt(2, lock.getDimension());
			ptmt.setInt(3, lock.getX());
			ptmt.setInt(4, lock.getY());
			ptmt.setInt(5, lock.getZ());
			ptmt.setInt(6, lock.getId());
			ptmt.executeUpdate();
		} catch (SQLException e) {
			plugin.getLogger().error("Problem trying to delete lock: " + e.getLocalizedMessage());
		} finally {
			try {
				if (ptmt != null) {
					ptmt.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException ignore) {
			}
		}
	}

	public Lock findById(MC_Location location) {
		Lock lock = null;
		int dimension = location.dimension;
		int x = location.getBlockX();
		int y = location.getBlockY();
		int z = location.getBlockZ();
		String sql = "SELECT * FROM lock WHERE DIMENSION = ? AND X = ? AND Y = ? AND Z = ?";
		try {
			connection = plugin.getDatabase().getConnection();
			ptmt = connection.prepareStatement(sql);
			ptmt.setInt(1, dimension);
			ptmt.setInt(2, x);
			ptmt.setInt(3, y);
			ptmt.setInt(4, z);
			resultSet = ptmt.executeQuery();
			if (resultSet.next()) {
				lock = new Lock(resultSet.getInt("ID"), resultSet.getString("OWNER"), resultSet.getInt("DIMENSION"), resultSet.getInt("X"), resultSet.getInt("Y"), resultSet.getInt("Z"));
			}
		} catch (SQLException e) {
			plugin.getLogger().error("Problem trying to find lock: " + e.getLocalizedMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (ptmt != null) {
					ptmt.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException ignore) {
			}
		}
		return lock;
	}
}
