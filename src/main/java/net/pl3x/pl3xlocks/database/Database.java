package net.pl3x.pl3xlocks.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import Pl3xLocks.MyPlugin;

public class Database {
	private MyPlugin plugin;
	private String driver = "org.sqlite.JDBC";
	private String database = "jdbc:sqlite:" + plugin.getPluginDir() + "locks.db";

	public Database(MyPlugin plugin) {
		this.plugin = plugin;
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() throws SQLException {
		Connection conn = null;
		conn = DriverManager.getConnection(database);
		return conn;
	}
}
