package net.pl3x.pl3xlocks.commands;

import java.util.List;

import net.pl3x.pl3xlocks.configuration.Alias;
import net.pl3x.pl3xlocks.configuration.Lang;
import Pl3xLocks.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Lock implements MC_Command {
	private MyPlugin plugin;

	public Lock(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.LOCK.get();
	}

	@Override
	public String getCommandName() {
		return "lock";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return plugin.colorize("&7/&8lock &e- &d" + Lang.LOCK_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		// TODO
		// XXX
		//
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("locks.lock");
	}
}
