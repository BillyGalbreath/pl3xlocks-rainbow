package Pl3xLocks;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.pl3x.pl3xlocks.Logger;
import net.pl3x.pl3xlocks.commands.Lock;
import net.pl3x.pl3xlocks.configuration.Config;
import net.pl3x.pl3xlocks.configuration.TextFile;
import net.pl3x.pl3xlocks.database.Database;
import net.pl3x.pl3xlocks.scheduler.Scheduler;
import PluginReference.MC_Block;
import PluginReference.MC_DamageType;
import PluginReference.MC_DirectionNESWUD;
import PluginReference.MC_Entity;
import PluginReference.MC_EventInfo;
import PluginReference.MC_GeneratedColumn;
import PluginReference.MC_HangingEntityType;
import PluginReference.MC_ItemFrameActionType;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_PotionEffectType;
import PluginReference.MC_Server;
import PluginReference.MC_Sign;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private static MyPlugin instance;
	private String pluginName = "Pl3xLocks";
	private String pluginVersion = "0.1-BETA1-SNAPSHOT";
	private String pluginDir = "plugins_mod" + File.separator + pluginName + File.separator;
	private Logger logger = null;
	private Scheduler scheduler = null;
	private Config config = null;
	private Database database;
	private MC_Server server;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.description = "Block protection made easy";
		info.eventSortOrder = 0.0f;
		info.pluginNamesINeedToGetEventsAfter = null;
		info.pluginNamesINeedToGetEventsBefore = null;
		return info;
	}

	public static MyPlugin getInstance() {
		return instance;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		instance = this;
		init();
		getLogger().info(getPluginName() + " v" + getVersion() + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		disable();
		getLogger().info("Plugin disabled.");
	}

	public Logger getLogger() {
		if (logger == null) {
			logger = new Logger(this);
		}
		return logger;
	}

	public MC_Server getServer() {
		return server;
	}

	public String getPluginDir() {
		return pluginDir;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public String getPluginName() {
		return pluginName;
	}

	public String getVersion() {
		return pluginVersion;
	}

	public Database getDatabase() {
		return database;
	}

	public Config getConfig() {
		if (config == null) {
			config = new Config(this);
		}
		return config;
	}

	public MC_Player getOfflinePlayer(String name) {
		List<MC_Player> offline = getServer().getOfflinePlayers();
		for (MC_Player player : offline) {
			if (player.getName().equalsIgnoreCase(name)) {
				return player;
			}
		}
		return null;
	}

	public MC_Player getOfflinePlayer(UUID uuid) {
		List<MC_Player> offline = getServer().getOfflinePlayers();
		for (MC_Player player : offline) {
			if (player.getUUID().equals(uuid)) {
				return player;
			}
		}
		return null;
	}

	public MC_Player getPlayer(UUID uuid) {
		List<MC_Player> online = getServer().getPlayers();
		for (MC_Player player : online) {
			if (player.getUUID().equals(uuid)) {
				return player;
			}
		}
		return null;
	}

	public List<String> getMatchingOnlinePlayerNames(String name) {
		List<String> matches = new ArrayList<String>();
		if (name == null) {
			name = "";
		}
		name = name.toLowerCase().trim();
		for (MC_Player player : getServer().getPlayers()) {
			if (player.getName().toLowerCase().startsWith(name)) {
				matches.add(player.getName());
			}
		}
		return matches;
	}

	public void sendMessage(MC_Player player, String message) {
		if (message == null || message.equals("")) {
			return;
		}
		if (player == null) {
			// Console or CommandBlock
			System.out.println(decolorize(message));
			return;
		}
		player.sendMessage(colorize(message));
	}

	public String colorize(String string) {
		return string.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}

	public String decolorize(String string) {
		return string.replaceAll("(?i)&([a-f0-9k-or])", "").replaceAll("(?i)\u00a7([a-f0-9k-or])", "");
	}

	private void init() {
		getConfig(); // initialize the config.ini file

		TextFile.copyFileFromJarToDisk(this, "lang-en.ini", true); // always overwrite default language file!
		TextFile.copyFileFromJarToDisk(this, "aliases.ini");

		database = new Database(this);

		scheduler = new Scheduler();

		getServer().registerCommand(new Lock(this));
	}

	private void disable() {
		scheduler.cancelAllRunnables();
		config = null;
		scheduler = null;
	}

	public void reload() {
		disable();
		init();
	}

	@Override
	public void onTick(int tickNumber) {
		getScheduler().tick();
	}

	@Override
	public void onPlayerLogin(String playerName, UUID uuid, String ip) {
	}

	@Override
	public void onPlayerLogout(String playerName, UUID uuid) {
	}

	@Override
	public void onInteracted(MC_Player plr, MC_Location loc, MC_ItemStack isHandItem) {
	}

	@Override
	public void onItemPlaced(MC_Player plr, MC_Location loc, MC_ItemStack isHandItem, MC_Location locPlacedAgainst, MC_DirectionNESWUD dir) {
	}

	@Override
	public void onBlockBroke(MC_Player plr, MC_Location loc, int blockKey) {
	}

	@Override
	public void onPlayerDeath(MC_Player plrVictim, MC_Player plrKiller, MC_DamageType dmgType, String deathMsg) {
	}

	@Override
	public void onPlayerRespawn(MC_Player player) {
	}

	@Override
	public void onPlayerInput(MC_Player player, String input, MC_EventInfo event) {
	}

	@Override
	public void onConsoleInput(String cmd, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptBlockBreak(MC_Player plr, MC_Location loc, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptPlaceOrInteract(MC_Player plr, MC_Location loc, MC_EventInfo ei, MC_DirectionNESWUD dir) {
	}

	@Override
	public void onAttemptExplosion(MC_Location loc, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptDamageHangingEntity(MC_Player plr, MC_Location loc, MC_HangingEntityType entType, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptItemFrameInteract(MC_Player plr, MC_Location loc, MC_ItemFrameActionType actionType, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptPotionEffect(MC_Player plr, MC_PotionEffectType potionType, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptPlayerTeleport(MC_Player player, MC_Location loc, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptPlayerChangeDimension(MC_Player plr, int newDimension, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptItemDrop(MC_Player plr, MC_ItemStack is, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptAttackEntity(MC_Player plr, MC_Entity ent, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptEntityDamage(MC_Entity ent, MC_DamageType dmgType, double amt, MC_EventInfo ei) {
	}

	@Override
	public void onGenerateWorldColumn(int x, int z, MC_GeneratedColumn data) {
	}

	@Override
	public void onAttemptPistonAction(MC_Location loc, MC_DirectionNESWUD dir, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptBlockFlow(MC_Location loc, MC_Block blk, MC_EventInfo ei) {
	}

	@Override
	public void onContainerOpen(MC_Player plr, List<MC_ItemStack> items, String internalClassName) {
	}

	@Override
	public void onAttemptPlayerMove(MC_Player plr, MC_Location locFrom, MC_Location locTo, MC_EventInfo ei) {
	}

	@Override
	public void onPacketSoundEffect(MC_Player plr, String soundName, MC_Location loc, MC_EventInfo ei) {
	}

	@Override
	public void onPlayerJoin(MC_Player player) {
	}

	@Override
	public void onSignChanging(MC_Player plr, MC_Sign sign, MC_Location loc, List<String> neWLines, MC_EventInfo ei) {
	}

	@Override
	public void onSignChanged(MC_Player plr, MC_Sign sign, MC_Location loc) {
	}

	@Override
	public void onAttemptEntitySpawn(MC_Entity ent, MC_EventInfo ei) {
	}

	@Override
	public void onServerFullyLoaded() {
	}

	@Override
	public void onAttemptHopperReceivingItem(MC_Location loc, MC_ItemStack is, boolean isMinecartHopper, MC_EventInfo ei) {
	}

	@Override
	public boolean onAttemptExplodeSpecific(MC_Entity ent, List<MC_Location> locs) {
		return false;
	}

	@Override
	public void onAttemptBookChange(MC_Player plr, List<String> bookContent, MC_EventInfo ei) {
	}
}
